package com.besaba.okomaru_server.junT58.WSAKv2;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wither;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

public class Main extends JavaPlugin implements Listener{


	public void onEnable(){
		getServer().getPluginManager().registerEvents(this, this);
		saveDefaultConfig();
	}

	@EventHandler
	public void onEntClick(PlayerInteractEntityEvent event){
		if(event.getPlayer().isOp()){
			Player player = event.getPlayer();
			if(player.getItemInHand().getType() != Material.AIR){
				if(player.getItemInHand().getType().equals(Material.INK_SACK)){
					if(event.getRightClicked().getType() == EntityType.WITHER){
						if(player.getItemInHand().getDurability() == 1){
							Wither wither = (Wither) event.getRightClicked();
							wither.setCustomName(ChatColor.RED + "レッドチームウィザーシンボル");
						}
						if(player.getItemInHand().getDurability() == 4){
							Wither wither = (Wither) event.getRightClicked();
							wither.setCustomName(ChatColor.BLUE + "ブルーチームウィザーシンボル");
						}
					}
				}
			}
		}
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return false;
		}
		if(args.length == 0){
			return false;
		}
		if(args[0].equalsIgnoreCase("red")){
			Player p = (Player) sender;
			Location loc = p.getLocation();
			String locs = loc.getBlockX() + "_" + loc.getBlockY() + "_" + loc.getBlockZ() + "_" + loc.getWorld().getName();
			getConfig().set("red", locs);
			saveConfig();
		}else if(args[0].equalsIgnoreCase("blue")){
			Player p = (Player) sender;
			Location loc = p.getLocation();
			String locs = loc.getBlockX() + "_" + loc.getBlockY() + "_" + loc.getBlockZ() + "_" + loc.getWorld().getName();
			getConfig().set("blue", locs);
			saveConfig();
		}else{
			return false;
		}
		return true;
	}

	@EventHandler
	public void onSpawn(CreatureSpawnEvent e){
		if(e.getEntityType() == EntityType.WITHER){
			Location loc = e.getEntity().getLocation();
			String locs = loc.getBlockX() + "_" + loc.getBlockY() + "_" + loc.getBlockZ() + "_" + loc.getWorld().getName();
			getLogger().info(locs);
			String red = getConfig().getString("red");
			String blue = getConfig().getString("blue");
			if(red == null){
				red = "ぬるぽ";
			}
			if(blue == null){
				blue = "ぬるぽ";
			}
			if(locs.equals(red)){
				Wither wither = (Wither) e.getEntity();
				wither.setCustomName(ChatColor.RED + "レッドチームウィザーシンボル");
			}else if(locs.equals(blue)){
				Wither wither = (Wither) e.getEntity();
				wither.setCustomName(ChatColor.BLUE + "ブルーチームウィザーシンボル");
			}
		}
	}

	@EventHandler
	public void onAttack(EntityDamageByEntityEvent event){
		if(event.getEntity().getType() == EntityType.WITHER){
			if(event.getDamager() instanceof Player){
				ScoreboardManager manager = Bukkit.getScoreboardManager();
				Scoreboard board = manager.getMainScoreboard();
				Player player = (Player) event.getDamager();
				Wither wither = (Wither) event.getEntity();
				String name = wither.getCustomName();
				Team playerteam = board.getPlayerTeam(player);
				if(playerteam == null){
					return;
				}
				if(name == null){
					name = "名前なし";
				}
				if(name.indexOf(ChatColor.RED + "レッドチーム") != -1){
					String teamname = playerteam.getName();
					if(teamname.equalsIgnoreCase("RedTeam")){
						event.setCancelled(true);
						player.sendMessage(ChatColor.RED + "自分のチームのシンボルを攻撃することはできません！");
						for(Player p : Bukkit.getServer().getOnlinePlayers()){
							if(p.isOp()){
								p.sendMessage(ChatColor.RED + player.getName() + "が自分のチームのシンボルを攻撃しています");
							}
						}
					}
				}else if(name.indexOf(ChatColor.BLUE +"ブルーチーム") != -1){
					String teamname = playerteam.getName();
					if(teamname.equalsIgnoreCase("BlueTeam")){
						event.setCancelled(true);
						player.sendMessage(ChatColor.RED + "自分のチームのシンボルを攻撃することはできません！");
						for(Player p : Bukkit.getServer().getOnlinePlayers()){
							if(p.isOp()){
								p.sendMessage(ChatColor.RED + player.getName() + "が自分のチームのシンボルを攻撃しています");
							}
						}
					}
				}
			}
		}
	}

}
